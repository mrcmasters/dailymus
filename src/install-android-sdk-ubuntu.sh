#!/bin/bash
#### GUIDE
# https://github.com/codepath/android_guides/wiki/Installing-Android-SDK-Tools
# SDK_VS="30.0.0"
# SDK_COMPILE="28"
SDK_VS="27.0.3"
SDK_COMPILE="27"
# SDK_URL="https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip?hl=pt-br"
SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip"
GRADLE_HOME="/opt/gradle"
ANDROID_HOME="/opt/android"
JAVA_HOME="/opt/jdk-10"
JAVA_FILE="jdk-10_linux-x64_bin_ri.tar.gz"
GRADLE_VS="7.0.2"
GRADLE_RELEASE="gradle-$GRADLE_VS-bin.zip" #gradle-4.7-bin.zip
GRADLE_DISTRIBUTION_URL="https://services.gradle.org/distributions/$GRADLE_RELEASE"
#Installs JAVA 10
# sudo mkdir $JAVA_HOME 
# cd $JAVA_HOME
# aws s3 cp s3://$S3_BACKUPS/zips/$JAVA_FILE
# aws s3api get-object --bucket $S3_BACKUPS --key zips/$JAVA_FILE ./
# tar -xzvf $JAVA_FILE
java -version

# Installs GRADLE
sudo wget $GRADLE_DISTRIBUTION_URL
sudo mkdir $GRADLE_HOME
sudo unzip -d $GRADLE_HOME $GRADLE_RELEASE
gradle -v

# Installs Android SDK
sudo rm -rf $ANDROID_HOME $GRADLE_HOME $JAVA_HOME
sudo mkdir $ANDROID_HOME && cd $ANDROID_HOME
sudo wget -O sdk.zip ${SDK_URL}
sudo unzip -d $ANDROID_HOME/ sdk.zip
sudo rm sdk.zip

if [ -d "$ANDROID_HOME/cmdline-tools" ]
then 
    pwd
    sudo mv $ANDROID_HOME/cmdline-tools/* $ANDROID_HOME/
fi

printf "UPDATE SDK\n"
echo y | $ANDROID_HOME/bin/sdkmanager --sdk_root=${ANDROID_HOME} --update
printf "INSTALL PLUGINS SDK\n"
echo y | sudo $ANDROID_HOME/bin/sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;${SDK_VS}" "platforms;android-${SDK_COMPILE}" "platform-tools" "extras;google;google_play_services" "extras;google;m2repository"
printf "LICENCE SDK\n"
echo y | $ANDROID_HOME/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses # && $ANDROID_HOME/bin/sdkmanager --update
echo y | $ANDROID_HOME/bin/sdkmanager --sdk_root=${ANDROID_HOME} --update

# avdmanager -v create avd -g google_apis -n Nexus_5_API_28 -k "system-images;android-28;google_apis;x86"

# Install npm packages with(--unsafe-perm)
# sudo npm install -g cordova@9.0.0 @ionic/cli@6.10.0 cordova-res
#ionic cordova
# ionic -v
# cordova -v
# cordova telemetry off

echo "export ANDROID_SDK_ROOT=$ANDROID_HOME" >> ~/.profile
echo "export ANDROID_HOME=\$ANDROID_SDK_ROOT" >> ~/.profile

echo "export PATH=\$ANDROID_HOME/bin:$GRADLE_HOME/gradle-$GRADLE_VS/bin:$JAVA_HOME/bin:\$PATH" >> ~/.profile
source ~/.profile