#!/bin/bash
volumes=$(docker volume ls --format '{{.Name}}')

for volume in $volumes
do
    sudo du -sh $(docker volume inspect --format '{{ .Mountpoint }}' $volume)
done
