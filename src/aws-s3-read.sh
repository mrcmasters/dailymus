#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

#set the default file to be open
if [[ -z $filename ]]
then
    colloredValue "Please provide a file name to download"
    exit
fi

# clean folder
if [[ -f "~/Downloads/$filename" ]]
then
    sudo rm ~/Downloads/$filename
fi


#download object from s3
aws s3api get-object --bucket $S3_GENERAL --key files/$filename ~/Downloads/$filename
#open downloaded file in directory default

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    xdg-open ~/Downloads/$filename
elif [[ "$OSTYPE" == "darwin"* ]]; then
    open ~/Downloads/$filename
fi