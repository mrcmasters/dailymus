#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

## create version CODE
if [ -z $versionName ]
then
    colloredValue "Please provide the version SEMVER" "warn"
    exit
fi

versionCode=0

major=$(echo $versionName | cut -d. -f1)
minor=$(echo $versionName | cut -d. -f2)
path=$(echo $versionName | cut -d. -f3)

if [ "$major" != "0" ]; then versionCode="$((versionCode + (major * 10000)))"; fi
if [ "$minor" != "0" ]; then versionCode="$((versionCode + (minor * 100)))"; fi
if [ "$path" != "0" ]; then versionCode=$((versionCode + path)); fi

echo $versionCode