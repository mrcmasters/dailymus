#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

## ====================================
## Validations
## ====================================
if [[ -z $repository ]]
then
    colloredValue "Please provide the repository name" "warn"
    exit
fi

if [[ -z $branchName ]]
then
    colloredValue "Please provide the branch Origin" "warn"
    exit
fi
if [[ -z $deployAction ]]
then
    colloredValue "Please provide custom action" "warn"
    exit
fi

GIT_USER=$MC_BB_USER
GIT_PASS=$MC_BB_PASSWORD
WORKSPACE=$MC_REPO_KEY

if [[ ! -z $workspace ]]; then WORKSPACE=$workspace; fi
if [[ ! -z $user ]]; then GIT_USER=$user; fi
if [[ ! -z $pass ]]; then GIT_PASS=$pass; fi

curl -X POST -is\
    -u $GIT_USER:$GIT_PASS \
    -H 'Content-Type: application/json' \
    https://api.bitbucket.org/2.0/repositories/$WORKSPACE/$repository/pipelines/ \
    -d "{
        \"target\": {
        \"selector\": {
            \"type\": \"custom\",
            \"pattern\": \"$deployAction\"
        },
        \"type\": \"pipeline_ref_target\",
        \"ref_name\": \"$branchName\",
        \"ref_type\": \"branch\"
        }
    }"