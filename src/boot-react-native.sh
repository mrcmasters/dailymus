#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

npx react-native init $name
cd $name

# React navigation
# https://reactnavigation.org/docs/getting-started
npm install @react-navigation/native \
npm install react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view \
# install stack strategy
npm install @react-navigation/stack

if [[ $isIOS = 'y' ]] 
then
    npx pod-install ios
fi

# React native elements
# https://reactnativeelements.com/docs
npm i react-native-elements --save
npm i --save react-native-vector-icons
react-native link react-native-vector-icons