#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

# @see https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D


## ====================================
## Validations
## ====================================
if [[ -z $repository ]]
then
    colloredValue "Please provide the repository name" "warn"
    exit
fi

if [[ -z $branchSource ]]
then
    colloredValue "Please provide the branch Origin" "warn"
    exit
fi

if [[ $branchSource = "master" ]]
then
    colloredValue "Invalid branch Origin" "warn"
    exit
fi

if [[ -z $branchDestiny ]]
then
    colloredValue "Please provide the branch Destiny" "warn"
    exit
fi

GIT_USER=$MC_BB_USER
GIT_PASS=$MC_BB_PASSWORD
WORKSPACE=$MC_REPO_KEY
VERSION=""
DELSOURCE="false"
MESSAGE="Merge $branchSource into $branchDestiny"

if [[ ! -z $workspace ]]; then WORKSPACE=$workspace; fi
if [[ ! -z $user ]]; then GIT_USER=$user; fi
if [[ ! -z $pass ]]; then GIT_PASS=$pass; fi
if [[ ! -z $prversion ]]; then VERSION=$prversion; fi;
if [[ ! -z $prmessage ]]; then MESSAGE=$prmessage; fi;
if [[ ! $branchSource =~ ^dev* ]]; then DELSOURCE="true"; fi

BASE_URL="https://api.bitbucket.org/2.0/repositories/$WORKSPACE/$repository"

errorResponse(){
    ERR=$(echo $1 | jq -r '.error.message')
    if [[ -z $ERR ]] || [[ $ERR = "null" ]]; then ERR=$1; fi
    echo $ERR
}
## ====================================
## Create a pull request
## ====================================

RESPONSE1=`curl $BASE_URL/pullrequests \
    -u $GIT_USER:$GIT_PASS \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "{
        \"title\": \"$MESSAGE\",
        \"source\": {
            \"branch\": {\"name\": \"$branchSource\"}
        },
        \"destination\": {
            \"branch\": {\"name\": \"$branchDestiny\"}
        }
    }"`

PULLREQUEST_ID=$(echo $RESPONSE1 | jq -r '.id')

if [[ -z $PULLREQUEST_ID ]] || [[ $PULLREQUEST_ID = "null" ]]
then
    colloredValue "Pull request fail: $(errorResponse "$RESPONSE1")" "err"
    exit
else
    colloredValue "Pull Request(#$PULLREQUEST_ID) successfully" "succ"
fi

## ==========================================
## Make merge of branch source to destinaiton
## ==========================================
RESPONSE2=`curl $BASE_URL/pullrequests/$PULLREQUEST_ID/merge\
    -u $GIT_USER:$GIT_PASS \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "{
        \"type\": \"pullrequest\",
        \"message\": \"$MESSAGE\",
        \"close_source_branch\": \"$DELSOURCE\",
        \"merge_strategy\": \"merge_commit\"
    }"`

COMMIT_HASH=$(echo $RESPONSE2 | jq -r '.merge_commit.hash')

if [ -z $COMMIT_HASH ] || [ $COMMIT_HASH = "null" ]
then
    colloredValue "Merge fail: $(errorResponse "$RESPONSE2")" "err"
    exit
else
    colloredValue "Merge($COMMIT_HASH) successfully" "succ"
fi


if [[ $branchDestiny = "master" ]] && [[ ! -z $VERSION ]]
then
    ## ==========================================
    ## Create tag from the recent merge
    ## ==========================================

    RESPONSE3=`curl $BASE_URL/refs/tags \
        -u $GIT_USER:$GIT_PASS \
        --request POST \
        --header "Content-Type: application/json" \
        --data "{
            \"name\" : \"$VERSION\",
            \"target\" : {\"hash\" : \"$COMMIT_HASH\"}
        }"`

    TAG=$(echo $RESPONSE3 | jq -r '.name')

    if [[ -z $TAG ]] || [[ $TAG = "null" ]]
    then
        colloredValue "tag fail: $(errorResponse "$RESPONSE3")" "err"
        exit
    else
        colloredValue "Tag($TAG) successfully" "succ"
    fi
fi

# git merge remotes/origin/branchName