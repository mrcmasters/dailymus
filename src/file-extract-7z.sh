#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

if [[ -z $filename ]]
then 
    colloredValue "Please provide the filename" "err"
	exit
fi

if [[ -z $folder ]]
then
    colloredValue "Please provide the folder path" "err"
    exit
fi

echo "$folder -> $filename"

if [[ $moveFile = 'y' ]]
then
    # move the file to the folder provided
    mv "$filename" "$folder"
fi

# go to the folder
cd $folder && \
# extract compress file with extension .7z
7z x $filename && \
# remove the compress file
rm "${folder}${filename}"