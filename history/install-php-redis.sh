#!/bin/bash

### validar se o php esta instalado
## install redis php 
brew install php@7.3
echo 'export PATH="/usr/local/opt/php@7.3/bin:$PATH"' >> ~/.zshrc
echo 'export PATH="/usr/local/opt/php@7.3/sbin:$PATH"' >> ~/.zshrc

## error Error: Could not symlink sbin/php-fpm /usr/local/sbin is not writable.
### validar se o diretorio /usr/local/sbin existe 
sudo mkdir /usr/local/sbin
sudo chown -R $(whoami) $(brew --prefix)/sbin

brew link php@7.3

git clone https://www.github.com/phpredis/phpredis.git && cd phpredis &&  phpize && ./configure && make && sudo make install

## Add extension=redis.so in your php.ini
echo "extension=redis.so" >> /usr/local/etc/php/7.3/php.ini

brew services restart php@7.3
make test

php -r "if (new Redis() == true){ echo \"\r\n OK \r\n\"; }"

brew install redis
redis-server /usr/local/etc/redis.conf