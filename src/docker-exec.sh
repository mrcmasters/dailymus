#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

if [[ -z $cname ]] ; then cname=$DEFAULT_DOCKER_ID; fi;
if [[ -z $ccommand ]]; then ccommand=$DEFAULT_DOCKER_EXEC; fi;

## revert filter in BG scripts to scape errors with space
ccommand=$(echo $ccommand | sed -e "s/__/ /g")

docker exec -t $cname bash -c "$ccommand"
