#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

if [[ -z $mode ]]; then mode="all"; fi

if [ $mode = 'all' ] || [ $mode = 'i' ]
then
    images=$(docker images -a -q)
    if [[ -z $images ]]
    then
        colloredValue "------------------Images already clean---------------------" "success"
    else
        colloredValue "------------------Clean Images---------------------" "warn"
        docker rmi -f $images
    fi
fi

if [ $mode = 'all' ] || [ $mode = 'v' ]
then 
    volumes=$(docker volume ls -q)
    
    if [[ -z $volumes ]]
    then
        colloredValue "------------------Volumes already clean---------------------" "success" 
    else
        colloredValue "------------------Clean Volumes---------------------" "warn"
        docker volume rm -f $volumes
    fi
fi