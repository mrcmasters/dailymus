#!/usr/bin/env node
const program = require('commander')
const shell = require('shelljs')
const package = require('./package.json')

program.version(package.version);

program
    .command('meip')
    .description('Display your IP address')
    .action(() => shell.exec('curl --silent ifconfig.me'))

program
    .command('dtnow')
    .description('Display pretty date')
    .action(() => shell.exec('date +"%A/%d in %B of %Y - %T"'))

program
    .command('spid')
    .requiredOption('--pidName <pidName>', 'The name of the process')
    .description('Display the porcess list')
    .action(({ pidName }) => shell.exec(`ps ux | grep '${pidName}' | awk '{print "PID:"FS$2}'`))

// Swap Memory - increase the memory swap size
program
    .command('swap-enlarge')
    .option('--size <size>', 'The size to increase the swap(number only)')
    .option('--save <save>', 'Save permanently (y)')
    .description('Increase the size of the swap memory')
    .action(({ size, save }) => {
        shell.exec(`${__dirname}/src/./swap-enlarge.sh "${size}" "${save}"`)
    })

// Swap Memory - increase the memory swap size
program
    .command('swap-clean')
    .option('--password <password>', 'The user password')
    .option('--force <force>', 'Force to clean')
    .description('Clean the swap memory')
    .action(({ size, save }) => {
        shell.exec(`${__dirname}/src/./swap-clean.sh "${size}" "${save}"`)
    })

// Rename file
program
    .command('pretty-name')
    .requiredOption('--filename <filename>', 'The file name')
    .description('Create a slug to the filename without special characters')
    .action(({ filename }) => {
        shell.exec(`${__dirname}/src/./general-script-rename.sh "${filename}"`)
    })


// Rename file
program
    .command('replace-name')
    .requiredOption('--filter <filter>', 'The string to be removed in filename')
    .description('Create a slug to the filter without special characters')
    .action(({ filter }) => {
        shell.exec(`${__dirname}/src/./replace-name.sh "${filter}"`)
    })
/**
* ------------------------------------------------
* Mysql Command
* ------------------------------------------------
*/
// Create a user and database in mysql
program
    .command('sql-add-user')
    .requiredOption('--dbname <dbname>', 'The name of the database')
    .requiredOption('--dbuser <dbuser>', 'The name of the user')
    .description('Script to create database and user of local database')
    .action(({ dbuser, dbname }) => {
        shell.exec(`${__dirname}/src/./sql-user-create.sh --dbname "${dbname}" --dbuser ${dbuser}`)
    })

// Delete a user and database in mysql
program
    .command('sql-rm-user')
    .requiredOption('--dbname <dbname>', 'The name of the database')
    .requiredOption('--dbuser <dbuser>', 'The name of the user')
    .description('Script to drop database and user of local database')
    .action(({ dbuser, dbname }) => {
        shell.exec(`${__dirname}/src/./sql-user-remove.sh --dbname "${dbname}" --dbuser ${dbuser}`)
    })
/**
 * ------------------------------------------------
 * Extract Command
 * ------------------------------------------------
*/
// Extract rar files
program
    .command('extract-rar')
    .option('--dir <dir>', 'The directory to read rar files')
    .description('Read and extract rar files in the currenct directory')
    .action(({ dir="" }) => {
        shell.exec(`${__dirname}/src/./file-extract-rar.sh "${dir}"`)
    })

// Extract 7z files
program
    .command('extract-7z')
    .requiredOption('--filename <filename>', 'The compress file name')
    .requiredOption('--folder <folder>', 'The destiny folder to extract the compressed archives')
    .option('--moveFile <moveFile>', '(boolean) The optional action to move the file to the folder')
    .description('Shell command to extract files compressed with extension .7z')
    .action(({ filename, folder, moveFile="" }) => {
        shell.exec(`${__dirname}/src/./file-extract-7z.sh --filename "${filename}" --folder "${folder}" --moveFile "${moveFile}"`)
    })

/**
 * ------------------------------------------------
 * S3 command
 * ------------------------------------------------
*/
// S3 read a file from bucket and open it
program
    .command('s3-read')
    .requiredOption('--filename <filename>', 'The file name to be download')
    .description('Download a file from s3 bucket')
    .action(({ filename }) => {
        shell.exec(`${__dirname}/src/./aws-s3-read.sh --filename ${filename}`, { silent: true }).stdout
    })

// S3 upload multipart with AWS CLI
program
    .command('s3-upload')
    .requiredOption('--filename <filename>', 'The file name to be upload')
    .requiredOption('--bucket <bucket>', 'The bucket name')
    .requiredOption('--folder <folder>', 'The folder in the bucket(prefix of object key)')
    .option('--attempt <attempt>', 'Attempt increment for case when upload to bucket fail|[print] = simulate execution without process upload')
    .option('--encode <encode>', 'Allow the process enconding(iconv) into ${fileName} to remove enconding present in (list files into folder)')
    .option('--acl <acl>', 'acl permission')
    .description(`
        Upload object to bucket s3 with a pretty name(slug), when the upload ends, based in the filename that you define in initial command,
        if found a file in sequence of {fileName} this script will be called recursively passing throught the next file found
        if the upload fail, this script will be called automatically try again upload the {fileName}
    `)
    .action(({ filename, bucket, folder, attempt="", encode ="", acl="" }) => {
        shell.exec(`${__dirname}/src/./aws-s3-upload.sh --fileName "${filename}" --bucket "${bucket}" --folder "${folder}" --attempt "${attempt}" --encode "${encode}" --acl "${acl}"`)
    })

// S3 upload multipart with AWS CLI
program
    .command('s3-clear-multipart')
    .requiredOption('--bucket <bucket>', 'The bucket name')
    .description('Clear the multipart upload from a bucket, create in uploads by CLI, Console, web')
    .action(({ bucket }) => {
        shell.exec(`${__dirname}/src/./aws-s3-clear.sh --bucket "${bucket}"`)
    })

program
    .command('s3-backup')
    .description('compress files of backup folder in local disk and upload the zip created to s3 bucket')
    .action(() => { shell.exec(`${__dirname}/src/./aws-s3-backup.sh`) })

// Run command for default clone git repository
program
    .command('git-clone')
    .requiredOption('--repository <repository>', 'The URL of the repository')
    .requiredOption('--credentialKey <credentialKey>', 'The key value of the repository account availables types: (git|gitbb)')
    .option('--cloneParams <cloneParams>', 'params of git clone to customize command')
    .option('--folderName <folderName>', 'The folder name of the cloned repository')
    .description('Command to make git clone and set local credentials for repository')
    .action(({ repository, credentialKey, cloneParams="", folderName="" }) => {
        shell.exec(`${__dirname}/src/./version-git-clone.sh --repository "${repository}" --credentialKey "${credentialKey}" --cloneParams "${cloneParams}" --folderName "${folderName}"`)
    })


/**
 * ------------------------------------------------
 * Docker command
 * ------------------------------------------------
*/
// Docker exec bash command in a container running
program
    .command('docker-exec')
    .option('--cname <cname>', 'The container name is required(default DEFAULT_DOCKER_ID)')
    .option('--ccommand <ccommand>', 'The custom command to be run (default DEFAULT_DOCKER_EXEC)')
    .description('Run a bash command in a active container')
    .action(({ cname="", ccommand="" }) => {
        shell.exec(`${__dirname}/src/./docker-exec.sh --cname "${cname}" --ccommand "${ccommand}"`)
    })

// Docker exec bash command in a container running
program
    .command('docker-clean')
    .option('--mode <mode>', 'The kind of remove(Default/all=images and volumes, i = images, v=volumes)')
    .description('Remove images and volumes')
    .action(({mode=""}) => {
        shell.exec(`${__dirname}/src/./docker-clear.sh --mode "${mode}"`)
    })
/**
 * ------------------------------------------------
 * Bitbucket Api Command
 * ------------------------------------------------
*/
// Create a Pull request and merge, if tag is defined, create the newly tag
program
    .command('bb-pullrequest')
    .requiredOption('--repository <repository>', 'The repository to create pull request+merge')
    .requiredOption('--source <source>', 'The branch origin to create merge')
    .requiredOption('--destiny <destiny>', 'The branch destination to create merge')
    .option('--message <message>', '(string default Merge {source} into {destiny}) The custom message to pullrequest+merge')
    .option('--tag <tag>', '(string default don\'t create tag) The tag value to create from the merging')
    .option('--workspace <workspace>', 'the workspace of the repository')
    .option('--user <user>', 'the username to login at bitbucket API')
    .option('--pass <pass>', 'the password to login at bitbucket API')
    .description('Shell command to create a pull request and merge it with destination branch')
    .action(({ repository, source, destiny, message = "", tag = "", workspace = "", user = "", pass = "" }) => {
        shell.exec(`${__dirname}/src/./bb-deploy-pr.sh --repository "${repository}" --branchSource "${source}" --branchDestiny "${destiny}" --prmessage "${message}" --prversion "${tag}" --workspace "${workspace}" --user "${user}" --pass "${pass}"`)
    })

// Create a Pull request and merge, if tag is defined, create the newly tag
program
    .command('bb-runpipe')
    .requiredOption('--repository <repository>', 'The repository to run the pipeline')
    .requiredOption('--source <source>', 'The branch name to run pipeline')
    .requiredOption('--deployAction <deployAction>', 'The custom action to run the pipeline')
    .option('--workspace <workspace>', 'the workspace of the repository')
    .option('--user <user>', 'the username to login at bitbucket API')
    .option('--pass <pass>', 'the password to login at bitbucket API')
    .description('Shell command to work with bitbucket pipelines')
    .action(({ repository, source, deployAction, workspace = "", user = "", pass = "" }) => {
        shell.exec(`${__dirname}/src/./bb-deploy-pipeline.sh --repository "${repository}" --branchName "${source}" --deployAction "${deployAction}" --workspace "${workspace}" --user "${user}" --pass "${pass}"`)
    })

// Runn command to set the local git credentials in repository
program
    .command('git-config')
    .requiredOption('--credentialKey <credentialKey>', 'The key value of the repository account availables types: (git|gitbb)')
    .description('Script to set credentials of git repository to commit changes')
    .action(({ credentialKey}) => {
        shell.exec(`${__dirname}/src/./version-git-config.sh --credentialKey "${credentialKey}"`)
    })

// Create a string as versionCode in conversion SEMVER
program
    .command('version-code')
    .requiredOption('--versionCode <versionCode>', 'the version SEMVER string')
    .description('Create a numeric version code from the version string SEMVER')
    .action(({ versionCode }) => {
        shell.exec(`${__dirname}/src/./version-create-code.sh --versionName "${versionCode}"`)
    })

program
    .command('version-npm')
    .requiredOption('--code <code>', 'The key of the version Definition to update(ma, mi, pa)')
    .description('Script to create tag and publish new version of npm packages')
    .action(({ code }) => {
        shell.exec(`${__dirname}/src/./version-npm-publish.sh --senver "${code}"`)
    })

program.parse(process.argv);