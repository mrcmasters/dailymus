#!/bin/bash

#### ================= not working here =================
# eval "export $(egrep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep -u $LOGNAME gnome-session)/environ)";
# DISPLAY=:0 notify-send "Test"
#### ================= not working here =================
EXTRA=''
if [[ ! -z $1 ]] && [[ -f $1 ]]; then EXTRA="-i $1"; fi

hour="$(date '+%H')"
pid=$(pgrep -u $LOGNAME gnome-session | head -n 1)
dbus=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$pid/environ | sed 's/DBUS_SESSION_BUS_ADDRESS=//' )
export DBUS_SESSION_BUS_ADDRESS=$dbus
export HOME=/home/$LOGNAME
export DISPLAY=:1

title="Hora do Elaboral"
msg="$hour Hrs - De uma pausa, beba uma água ;)"

if [[ "$hour" = "10" ]] || [[ "$hour" = "15" ]]
then
    title="Hora do cafe"
    msg="Hora do cafe das $hour"
elif [[ "$hour" = "12" ]]
then
    title="Bom almoço"
    msg="Meio dia, é hora de almoçar!"
elif [[ "$hour" = "18" ]]
then
    title="Bom descanço"
    msg="Mais um dia finalizado!"
fi

if [[ ! -z $2 ]]
then
    title=$2
fi

if [[ ! -z $3 ]]
then
    msg=$3
fi

/usr/bin/notify-send "$title" "$msg" $EXTRA