#!/bin/bash

# https://terminalroot.com.br/2015/07/30-exemplos-do-comando-sed-com-regex.html
ext=`echo $1 | sed 's/.*\(\.[a-zA-Z0-9]*\)$/\1/'`
filename=`echo $1 \
    | sed "s/${ext}$//"\
    | sed "s/[[:punct:]]//g" \
    | sed "s/480p//g" \
    | sed "s/720p//g" \
    | sed "s/1080p//g" \
    | sed "s/ /-/g" \
    | sed "s/[[:punct:]]$//g" \
    | sed "s/[áâàãä]/a/g"\
    | sed "s/[éêèë]/e/g"\
    | sed "s/[íîìï]/i/g"\
    | sed "s/[oóôòõö]/o/g"\
    | sed "s/[úûùü]/u/g"\
    | sed "s/[ç]/c/g"\
    | sed "s/[ñ]/n/g"\
    | sed "s/aa/a/g"
    # | iconv -f ISO-8859-1 -t UTF-8\
    # | sed "s/[Ì§]//g"
`
echo "$filename$ext"