#!/bin/bash

RESET="\033[0m"
COLOR="\033[1;34m"
COLOR2="\033[4;33m"

# for dir in "$1"/*; do
#     ext=`echo $dir | sed 's/.*\(\.[a-zA-Z0-9]*\)$/\1/'`;
#     newName=`echo $dir | sed 's/.*\(S[0-9]*E[0-9]*\).*/\1/'`;
#     read -p "$(echo -e $COLOR"Rename $newName$ext(y/n): "$RESET)" confirmation
    
#     if [ $confirmation = 'y' ]
#     then
#         mv "$dir" "$1/$newName$ext"
#     fi
# done
founded='n'
for dir in ./*; do
    if [[ $dir =~ "$1" ]]
    then
        if [[ $founded = 'n' ]]; then founded='y'; fi; 
        newName=`echo $dir | sed "s/$1//"`
        echo -e $COLOR"OLD: $dir\t$COLOR2""NEW: $newName"$RESET
    fi
done

echo $founded
if [[ $founded = 'y' ]]
then
    read -p "$(echo -e $COLOR"Do you want rename files?[y]"$RESET)" confirmation
    if [[ $confirmation = 'y' ]]
    then
        for dir in ./*; do
            if [[ $dir =~ "$1" ]]
            then 
                newName=`echo $dir | sed "s/$1//"`
                mv "$dir" "$newName"
            else
                echo $dir
            fi
        done
    fi
else
    echo "Directory already rename by filter[$1]"
fi