#!/bin/bash
sudo npm uninstall -g cordova
sudo npm uninstall -g ionic
sudo npm uninstall -g @ionic/cli

if [ "$1" = "old" ]
then
    sudo npm install -g ionic@3.2.0 ## more ionic 2.2.2
    sudo npm install -g cordova@8.1.2 ## more cordova 6.5.0
else
    sudo npm install -g cordova@9.0.0 @ionic/cli@6.10.0
fi
