#!/bin/bash
folder=""

if [[ ! -z $1 ]]
then
    dir=$(echo $1 | sed -e "s/__/ /g")
    folder="/${dir}"
fi

for file in ."$folder"/*
do
    unrar x "${file}" ./
done