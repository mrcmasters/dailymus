#!/bin/bash
source .default-variables

if [[ -z $bucketName ]]
then
    colloredValue "Please provide the bucketName" "err"
    exit
fi

aws s3api get-bucket-lifecycle --bucket $bucketName