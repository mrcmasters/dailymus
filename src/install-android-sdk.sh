#!/bin/bash
#### GUIDE
# https://github.com/codepath/android_guides/wiki/Installing-Android-SDK-Tools
# SDK_VS="30.0.0"
# SDK_COMPILE="28"
SDK_VS="27.0.3"
SDK_COMPILE="27"

if [ -z "$1" ]
then 
    FOLDER=$FOLDER_PROJECT
else
    FOLDER=`echo ~/`
fi
  
SDK_FOLDER="android-sdk"
ANDROID="${FOLDER}${SDK_FOLDER}"
# SDK_URL="https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip?hl=pt-br"
SDK_URL="https://dl.google.com/android/repository/commandlinetools-mac-6609375_latest.zip"

sed -i '' -e '/FOLDER_PROJECT/d' ~/.bash_profile
sed -i '' -e '/ANDROID_HOME/d' ~/.bash_profile
sed -i '' -e '/ANDROID_SDK_ROOT/d' ~/.bash_profile
sed -i '' -e "/build-tools\/${SDK_VS}/d" ~/.bash_profile

echo "export FOLDER_PROJECT=\"${FOLDER}\"" >> ~/.bash_profile
echo "export ANDROID_HOME=\"${ANDROID}\"" >> ~/.bash_profile
echo "export PATH=\"\${PATH}:\$ANDROID_HOME/emulator\"" >> ~/.bash_profile
echo "export PATH=\"\${PATH}:\$ANDROID_HOME/tools/bin\"" >> ~/.bash_profile
echo "export PATH=\"\${PATH}:\$ANDROID_HOME/platform-tools\"" >> ~/.bash_profile

source ~/.bash_profile

rm -rf $ANDROID
mkdir $ANDROID
cd $ANDROID

wget -O sdk.zip $SDK_URL
unzip -d $ANDROID sdk.zip
rm sdk.zip

printf "UPDATE SDK\n"
$ANDROID/tools/bin/sdkmanager --sdk_root=${ANDROID} --update
printf "INSTALL PLUGINS SDK\n"
$ANDROID/tools/bin/sdkmanager --sdk_root=${ANDROID}\
    "platforms;android-${SDK_COMPILE}"\
    "build-tools;${SDK_VS}"\
    "platform-tools"\
    "extras;google;m2repository"\
    "extras;android;m2repository"
    # "extras;google;google_play_services" 
    # "system-images;android-${SDK_COMPILE};google_apis;x86_64"
printf "LICENCE SDK\n"
$ANDROID/tools/bin/sdkmanager --sdk_root=${ANDROID} --licenses # && $ANDROID/tools/bin/sdkmanager --update
$ANDROID/tools/bin/sdkmanager --sdk_root=${ANDROID} --update

# create a avd
avdmanager -v create avd -g google_apis -n Nexus_5_API_$SDK_COMPILE -k "system-images;android-${SDK_COMPILE};google_apis;x86"
##############################################################################
### after create the avd you must edit the file ~/.android/avd/Nexus_5_API_$SDK_COMPILE/config.ini
### and in the line of 'image.sysdir.1' put the absolute path to android SDK
##############################################################################

### extra commands
### DELETE AN AVD
# avdmanager delete avd -n "NAME" 

## LIST EMULATORS
# emulator -list-avds

## run android emulator
# emulator -avd Nexus_5_API_27