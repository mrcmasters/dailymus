#!/bin/bash
memPerc=`awk '/^Mem/ {printf("%u", 100*$3/$2);}' <(free -m)`
swapPerc=`awk '/^Swap/ {printf("%u", 100*$3/$2);}' <(free -m)`

# messages="MEM: $memPerc, SWAP: $swapPerc - $1"

cleanup(){
    start=$(date +"%s")
    messages="memory $memPerc% swap $swapPerc% "
    if [[ $(whoami) != 'root' ]]
    then
        messages+="Log in Root "
        echo $1 | sudo -S su -c  "echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a && messages+='Ram-cache and Swap Cleared '"
    else
        messages+="On Root "
        echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a && messages+='Ram-cache and Swap Cleared '
    fi 
    endt=$(date +"%s")
    elapsedTime=`echo $(expr $endt - $start) | awk '{print strftime("%M:%S", $0)}'`
    messages+="Script time: $elapsedTime"
    if [[ -e /home/$USER/bin/src/elaboral.sh ]]
    then
        /home/$USER/bin/src/elaboral.sh '/home/marlon/Desktop/mardozux.png' 'Memory Cleaned' "$messages"
    fi
    echo -e "$(date +"%A-%Y-%m-%d %H:%M:%S") - [$messages]"
}

if [[ $swapPerc > 95 ]] || [[ ! -z $2 ]]
then
    cleanup $1
fi