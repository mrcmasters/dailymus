#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

_jq() {
    echo ${row} | base64 --decode | jq -r ${1}
}

response=$(aws s3api list-multipart-uploads --bucket "$bucket")

for row in $(echo "${response}" | jq -r '.Uploads[] | @base64'); do
    UploadId=$(_jq '.UploadId')
    Key=$(_jq '.Key')

    if [[ $(_empty $UploadId) -eq 1 ]] || [[ $(_empty $Key) -eq 1 ]]
    then
        colloredValue "Fail to list: $row" "err"
        break
    fi
    
    colloredValue "($Key) - $UploadId" "succ"
    aws s3api abort-multipart-upload --bucket "$bucket" --upload-id $UploadId --key "$Key"
done