#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

if [[ -z $dbname ]]
then
    colloredValue "Please provide the database name" "err"
	exit
fi

if [[ -z $dbuser ]]
then
    colloredValue "Please provide the database name" "err"
	exit
fi

echo "CREATE DATABASE $dbname;" > temp.sql
echo "CREATE USER '$dbuser'@'$MC_DB_HOST' IDENTIFIED WITH mysql_native_password BY '$dbuser';" >> temp.sql
echo "GRANT ALL PRIVILEGES ON $dbname.* TO '$dbuser'@'$MC_DB_HOST';" >> temp.sql
echo "GRANT ALL ON *.* TO '$dbuser'@'$MC_DB_HOST' WITH GRANT OPTION;" >> temp.sql
echo "Flush Privileges" >> temp.sql

mysql -h$MC_DB_HOST -u$MC_DB_USER -p$MC_DB_PASSWORD < temp.sql && rm temp.sql 