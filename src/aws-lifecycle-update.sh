#!/bin/bash
source .default-variables

if [[ -z $bucketName ]]
then
    colloredValue "Please provide the bucketName" "err"
    exit
fi

if [[ -z $fileName ]]
then
    colloredValue "Please provide the fileName" "err"
    exit
fi

aws s3api put-bucket-lifecycle-configuration --bucket $bucketName --lifecycle-configuration "file://${fileName}"