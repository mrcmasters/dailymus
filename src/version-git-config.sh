#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

case "$credentialKey" in
    "git")
        username=$MC_GIT_USER
        email=$MC_GIT_EMAIL
        name="GitHub"
        ;;
    "gitbb")
        username=$MC_BB_USER
        email=$MC_BB_EMAIL
        name="Bitbucket"
        ;;
    *)
        colloredValue "The kind of config is not valid" "warn"
        exit
        ;;
esac

echo "--------------------------DEFINE CONFIGS $name--------------------------"
git config --local user.name $username &&
git config --local user.email $email &&
git config --list