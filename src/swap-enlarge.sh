#!/bin/bash
sizeValue='1'
nameValue='/swap.img'
if [[ -n $1 ]]; then sizeValue=$1; fi
if [[ -n $3 ]]; then nameValue=$3; fi

if [[ -e $nameValue ]]
then
    echo "Please remove the $nameValue file as sudo before continue"
else
    sudo fallocate -l "${sizeValue}G" $nameValue\
        && sudo chmod 600 $nameValue\
        && sudo mkswap $nameValue\
        && sudo swapon $nameValue\
        && free -m

    if [[ $2 = 'y' ]]
    then
        echo "$nameValue none swap sw 0 0" | sudo tee -a /etc/fstab
    fi
fi