#!/bin/bash
if [[ ! -d $DIR_BACKUP ]]
then
    mkdir $DIR_BACKUP
fi

for px in $DIR_BACKUP/*
do
    filename=`echo $(basename $px)`
    ext=`echo $filename | sed 's/.*\(\.[a-zA-Z0-9]*\)$/\1/'`
    
    if [[ $ext = ".zip" ]]; then continue; fi;
    
    destiny="$DIR_BACKUP/$filename.zip"
    # compress folder in zip file
    zip -r -X $destiny $px -x ".DS_Store $excluded"
    # upload the newly zip file to backup bucket
    aws s3 cp $destiny "s3://${S3_BACKUPS}/zips/$filename"
    # delete zip file from local disk
    sudo rm $destiny
done