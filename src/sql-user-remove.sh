#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

if [[ -z $dbname ]]
then
    colloredValue "Please provide the database name" "warn"
	exit
fi

if [[ -z $dbuser ]]
then
    colloredValue "Please provide the database name" "warn"
	exit
fi

echo "DROP DATABASE IF EXISTS $dbname;" > temp.sql
echo "DROP USER IF EXISTS '$dbuser'@'$MC_DB_HOST';" >> temp.sql
# echo "REVOKE ALL PRIVILEGES ON $dbname.* FROM '$dbuser'@'$MC_DB_HOST';" > temp.sql

mysql -h$MC_DB_HOST -u$MC_DB_USER -p$MC_DB_PASSWORD < temp.sql && rm temp.sql 