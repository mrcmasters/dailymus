#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

echo "$repository - $credentialKey - $cloneParams - $folderName"
if [[ -z $repository ]]
then
    colloredValue "Please provide the repository PATH" "warn"
    exit 1
fi

if [[ -z $credentialKey ]]
then
    colloredValue "Please provide the Credentials Key of account in repository" "warn"
    exit
fi

if [[ -z $folderName ]]
then
    folderName=`echo $(basename $repository) | sed -e 's/.git//g'`
fi

git clone $cloneParams $repository $folderName && \
    cd $folderName && \
    $BASEDIR/./version-git-config.sh --credentialKey $credentialKey
