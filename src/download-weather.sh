#!/bin/bash
# ${execi 300 l=${template9}; l=${l%%_*}; curl -s "api.openweathermap.org/data/2.5/forecast/daily?APPID=${template6}&id=${template7}&cnt=5&units=${template8}&lang=$l" -o ~/.cache/forecast.json}\
# ${execi 300 l=${template9}; l=${l%%_*}; curl -s "api.openweathermap.org/data/2.5/weather?APPID=${template6}&id=${template7}&cnt=5&units=${template8}&lang=$l" -o ~/.cache/weather.json}\
# API Key
apikey="52988146207f81ce31ebc54ff6a4ad16"
city="6167865" # City ID
unit="imperial" # Temp Unit (default, metric, imperial)
lang="en_US.UTF-8" # Locale (e.g. "es_ES.UTF-8"), Leave empty for default

if [[ $1 = 'home' ]]
then
    city="3472518"
    lang="pt_BR.UTF-8"
elif [[ $1 = 'usa' ]]
then
    city="5128581"
elif [[ $1 = 'london' ]]
then
    city='2643743'
elif [[ $1 = 'paris' ]]
then
    city='2988507'
elif [[ $1 = 'italy' ]]
then
    city='3175395'
fi

echo "Downloading forecast..."
curl -s "api.openweathermap.org/data/2.5/forecast/daily?APPID=${apikey}&id=${city}&cnt=5&units=${unit}&lang=$lang" -o ~/.cache/forecast.json
echo "Downloading weather..."
curl -s "api.openweathermap.org/data/2.5/weather?APPID=${apikey}&id=${city}&cnt=5&units=${unit}&lang=$lang" -o ~/.cache/weather.json
weather=`echo ~/.cache/weather.json`
forecast=`echo ~/.cache/forecast.json`

# echo "api.openweathermap.org/data/2.5/forecast/daily?APPID=${apikey}&id=${city}&cnt=5&units=${unit}&lang=$lang"
echo "$(jq -r .name $weather) ($(jq -r .sys.country $weather)) - $(jq -r .weather[0].main $weather), $(jq -r .weather[0].description $weather) [$(jq -r .weather[0].icon $weather)]"

jq -c '.list[]' $forecast | while read i; do
    echo "$(echo $i | jq .dt | awk '{print strftime("%A(%d) %m", $0)}') [$(echo $i | jq '.temp.min' | awk '{print int($1+0.5)}')°C/$(echo $i | jq '.temp.max' | awk '{print int($1+0.5)}')°C]"
done