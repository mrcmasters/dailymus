

# Shellscript to automation of daily tasks

#### add notification by cron
Use the notify-send to send you a notification for a break time
```shell
## create a Job in cron to call this file
ABSOLUTE_PATH_THIS_PROJECT/src/elaboral.sh [ICON_IMAGE_PATH]

## edit the crontab of user
crontab -e

## add the Job like the example below, 
## this example runs for every hour, at the 0min, from 8h to 18h, from Monday to Friday(
## feel free to implement and change on your way)
0 8-18 * * 1-5 ABSOLUTE_PATH_THIS_PROJECT/src/elaboral.sh "ABSOLUTE_PATH_TO_IMAGE"
```

#### Setup your environment in Ubuntu
Install packages and dependencies commonly used by fullstack developer on a daily basis
```bash
ABSOLUTE_PATH_THIS_PROJECT/src/boot-ubuntu-dev.sh
```
## Thrid part dependencies
* **jq:** Process and work with JSON files
* **aws:** CLI AWS for work with the commands of this service
* **rar:** Enable extract rar files
* **mysql:** CLI to work with mysql database
* **curl:** CLI to run process to work with api as bitbucket

## Create configuration
Copy the file '.my_bash_profile.example' and replace the variables exported with your personal credentials,
add to the reference for this file in your '.bash_profile' or the file name who contains your terminal session information
```
cp .my_bash_profile.example YOUR_LOCATION/.my_bash_profile
```

## Recomendation

Always create the newly command files in the standard 'category-commandName.sh',  
try use short names without use special char(e.g: _, -, .) for two word name use cameocase

## default-variables usage

### register in your script base
You must set this lines in your shellScript to enable the below commands
```
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables
```

### print a collored message
```
colloredValue [MESSAGE] [KIND=err|warn|succ]
```

### List all available command
```
$ dailymus
```

### Principal commands

#### S3 Upload
Upload the files in directory recursively, rename the filename in bucket as slug name
```
$ dailymus s3-upload \
    --filename "The inicial name" \
    --bucket "Your bucket" \
    --folder "the folder destiny" \
    --attempt "increment number to retry, or 'print' for debug"
```

#### Bitbucket PullRequest
Automate the process of pull request, merge and tag in bitbucket repository
```
$ dailymus bb-pullrequest\
    --repository "Repository name" \
    --source "Branch Origin" \
    --destiny "Branch Destiny" \
    --message "the merge commit message" \
    --tag "the tag name" \
    --workspace "workspace of repository" \
    --user "username of repository" \
    --pass "passoword of repository" \
```

#### Docker exec
Run a simple bash command into container
```
$ dailymus docker-exec --cname "container name" --ccommand "command to be execute"
```

#### Docker clean
Remove images and volumes based in the --mode, the default if 'all' clean everything
```
$ dailymus docker-clean --mode "all|v|i"
```