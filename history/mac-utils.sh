#!/bin/bash
params=$2;

case "$1" in
	"shfinder")
		# exibir arquivos ocultos
		## apos esta acao reinicie o finder para funcionar, va no menu MAC -> forcar encerrar
		if [ -z $params ]; then params=false; fi;
		defaults write com.apple.finder AppleShowAllFiles -bool $params
		;;
	"xcodectl")
		xcrun simctl delete unavailable
		;;
	"enbany")
		# Script para habilitar ou desabilitar apps de qualquer lugar no Mac
		# 
		# Por padrão, a chamada sem argumentos libera apps de qualquer lugar.
		# Se quiser desabilitar, passe o parâmetro "--no"
		#
		# @link http://osxdaily.com/2016/09/27/allow-apps-from-anywhere-macos-gatekeeper/
		if [ "$params" == "--no" ]
		then
			sudo spctl --master-enable
		else
			sudo spctl --master-disable
		fi
		;;
esac