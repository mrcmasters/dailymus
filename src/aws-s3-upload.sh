#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

# https://medium.com/@codenameyau/how-to-copy-and-paste-in-terminal-c88098b5840d
# pbcopy
# pbpaste

fileName=$(echo $fileName | sed -e "s/__/ /g")
helpExp="^(help|h|\-h|\-\-help)$"

# ----------------------------------------------------------------
#  walk through the files of the current directory, 
#  and looking by the next file in the currently uploading file
#  $1 the name of the current file processing in script
#  $2 the value to prepare the $1 encoding, removing the accents that the $BASEDIR/./general-script-rename.sh don't remove
# ----------------------------------------------------------------
getNextFile(){
    found=$($BASEDIR/./general-script-rename.sh "$1")

    if [[ "$2" == "true" ]]
    then
        found=$(echo $found | iconv -f utf8 -t ascii//TRANSLIT//IGNORE)
    fi
    
    i=0
    index=-1
    allowAdd="0"
    
    for fx in ./*
    do
        filter=$(echo $fx | sed "s/^\.\///g")
        fileCls=$($BASEDIR/./general-script-rename.sh "$filter" | iconv -f utf8 -t ascii//TRANSLIT//IGNORE)

        if [[ $found == $fileCls ]]
        then
            allowAdd="1"
            index=$i
        fi

        if [[ $allowAdd == "1" ]]
        then
            items[$i]=$(echo "$filter" | sed "s/ /____/g")
            (( i++ ))
        fi

        if [ $i -eq 2 ]; then break; fi;
    done

    size=${#items[@]}
    
    if [ $size -eq 2 ]
    then
        value=${items[1]}
        value=$(echo "$value" | sed "s/____/ /g")
        echo $value
    fi
    echo ""
}

# verify if local file not exits
if [[ ! -f "$fileName" ]]
then
    echo "File not found $fileName"
    exit
fi

uri="s3://$bucket/$folder"
newFilename=$($BASEDIR/./general-script-rename.sh "$fileName")

if [[ "$encode" == "true" ]]
then
    newFilename=$(echo $newFilename | iconv -f utf8 -t ascii//TRANSLIT//IGNORE)
fi

if [[ "$attempt" == "print" ]]
then
    next=$(getNextFile "$fileName" "$encode")
    colloredValue "URI: $uri/$newFilename [$next]"
    
    if [ ! -z "$next" ]
    then
        $BASEDIR/./aws-s3-upload.sh \
        --fileName "$next" \
        --bucket "$bucket" \
        --folder "$folder" \
        --attempt "$attempt" \
        --encode "true" \
        --acl "$acl"
        exit
    fi
    exit
elif [[ -z $attempt ]]
then
    attempt=1
fi

# terminate recursivity
if [[ $attempt -gt 3 ]]; then exit; fi;

# clean up the all multipart in bucket 
$BASEDIR/./aws-s3-clear.sh --bucket $bucket

# print the start message upload
colloredValue "Start-URI: $uri/$newFilename, Attempt: $attempt, encoded: $encode, at: $(date +'%H:%M:%S')" "warn"

# upload object to s3 bucket and path defined
aws s3 cp "$fileName" "$uri/$newFilename" --acl "$acl"

# check if the object was successfullt create in bucket
size=$(aws s3 ls $uri/$newFilename --recursive --summarize | grep "Total Objects: " | sed 's/[^0-9]*//g')

if [[ -z $size ]] || [ "$size" = "0" ]
then
    ## try to upload the file again, for three times
    $BASEDIR/./aws-s3-upload.sh \
            --fileName "$fileName" \
            --bucket "$bucket" \
            --folder "$folder" \
            --attempt $((attempt+1)) \
            --encode "$encode" \
            --acl "$acl"
else
    colloredValue "End-URI: https://$bucket.s3.amazonaws.com/$folder/$newFilename, Finish at: $(date +'%H:%M:%S')" "succ"
    
    #go to the next file
    next=$(getNextFile "$fileName" "$encode")
    # colloredValue "Next: $next" "succ"
    
    if [[ ! -z $next ]]
    then
        $BASEDIR/./aws-s3-upload.sh \
            --fileName "$next" \
            --bucket "$bucket" \
            --folder "$folder" \
            --attempt "" \
            --encode "true" \
            --acl "$acl"
    fi
fi