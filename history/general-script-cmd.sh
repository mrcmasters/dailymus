#!/bin/bash
source .default-variables

if [[ -z $filename ]]
then
    colloredValue "Please provide the database name" "err"
	exit
fi

printf "#!/bin/bash\nsource .default-variables" > $DIR_DAILY_SCRIPT/$filename.sh\
    && chmod u+x $DIR_DAILY_SCRIPT/$filename.sh\
    && cp $DIR_DAILY_SCRIPT/docs/template.txt $DIR_DAILY_SCRIPT/docs/$filename.txt\
    && sed -i '' -e "s/{FILE}/$filename/" $DIR_DAILY_SCRIPT/docs/$filename.txt