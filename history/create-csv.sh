#!/bin/bash

RESET="\033[0m"
COLOR="\033[1;34m"
uri="https://${S3_GENERAL}.s3.amazonaws.com/$2"

if [ ! -e "$s3moviefile/$1.csv" ]
then
    touch "$s3moviefile/$1.csv"
fi

#https://mcpovies.s3.amazonaws.com/movie/resident-evil/resident-evil-Degeneracao.mkv
for dir in ./*; do
    newFilename=$(~/bin/src/./general-script-rename.sh "$dir")
    url="${uri}/${newFilename}"
    echo $url >> "$s3moviefile/$1.csv"
done
