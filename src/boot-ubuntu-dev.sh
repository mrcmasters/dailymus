#!/bin/bash

#### ====================================== INSTALL ZSH FIRST ================================================= ####
https://medium.com/@ivanaugustobd/your-terminal-can-be-much-much-more-productive-5256424658e8

profilesh=`echo ~/.bashrc`
if [[ -f ~/.zshrc ]]
then
    profilesh=`echo ~/.zshrc`
fi

## ======================================= INSTALL DOCKER ============================================
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# enable docker in systemctl
sudo systemctl enable docker
# run a example to see if everything it's OK
docker run hello-world 

echo -e '
alias docker="USERNAME=$(id -un) USER_ID=$(id -u) GROUP_ID=$(id -g) docker"
alias docker-compose="USERNAME=$(id -un) USER_ID=$(id -u) GROUP_ID=$(id -g) docker-compose"
' >> $profilesh

source $profilesh
## ======================================= INSTALL SODEPS ============================================
sudo apt-get install -y jq\
                        git\
                        unzip\
                        wget\
                        nodejs\
                        npm\
                        php\
                        # php7.4-curl\
                        # php-xml\
                        php-xmlwriter

## ======================================= INSTALL NVM ============================================
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
# install node version 12
nvm install v12

# install node g packages
npm install -g gulp bower gulp-less

## install composer
# curl -sS https://getcomposer.org/installer -o composer-setup.php
# sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

echo "PHP: $(php -v)\n"
echo "DOCKER: $(docker -v)\n"
echo "GIT: $(git -v)\n"
echo "NODE: $(node -v)\n, NPM: $(npm -v)\n"

# create new ssh to connect in repositories
ssh-keygen -b 4096