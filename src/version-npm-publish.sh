#!/bin/bash
BASEDIR=$(dirname "$0")
source $BASEDIR/.default-variables

versionName=''
case "$senver" in
    "ma")
        versionName="major"
        ;;
    "mi")
        versionName="minor"
        ;;
    "pa")
        versionName="patch"
        ;;
    *)
        colloredValue "invalid Semantic version of package" "err"
        exit
        ;;
esac

npm version $versionName\
    && npm publish --access public\
    && git push origin master --tags